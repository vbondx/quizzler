import UIKit

class ViewController: UIViewController {
    
    let allQuestions = QuestionBank()
    var pickedAnswer : Bool = false
    var questionNumber : Int = 0
    var score : Int = 0
    var progressValue : Int = 0
    var isLastQuestion: Bool {
        return questionNumber == allQuestions.list.count - 1
    }
    
    //Place your instance variables here
    
    
    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet var progressBar: UIView!
    @IBOutlet weak var progressLabel: UILabel!
    
    enum AlertButtonStates: String {
        case nextQuestion = "Next question"
        case lastQuestion = "Restart"
    }
    
    enum AlertMessageStates: String {
        case rightAnswer = "It's right answer"
        case wrongAnswer = "It's wrong answer"
        case lastQuestion = ". You've finished all the questions, do you want to start over? "
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let firstQuestion = allQuestions.list[0]
        questionLabel.text = firstQuestion.questionText
        score = 0
        progressValue = questionNumber + 1
        updateUI()
        
    }


    @IBAction func answerPressed(_ sender: AnyObject) {
        if sender.tag == 1 {
            pickedAnswer = true
        } else if sender.tag == 2 {
            pickedAnswer = false
        }
        checkAnswer()
        nextQuestion()
    }
    
    
    func updateUI() {
        scoreLabel.text = "Score: \(score)"
        progressLabel.text = "\(questionNumber + 1)/13"
    }
    

    func nextQuestion() {
        if !isLastQuestion{
            questionNumber = questionNumber + 1
            questionLabel.text = allQuestions.list[questionNumber].questionText
            updateUI()
        } else {
            startOver()
        }
    }
    
    
    func checkAnswer() {
        let correctAnswer = allQuestions.list[questionNumber].answer
        if correctAnswer == pickedAnswer {
            print("You got it")
            score = score + 1
            updateUI()
            var messageTitle = AlertMessageStates.rightAnswer.rawValue
            var buttonTitle = AlertButtonStates.nextQuestion.rawValue
            if isLastQuestion {
                messageTitle += AlertMessageStates.lastQuestion.rawValue
                buttonTitle = AlertButtonStates.lastQuestion.rawValue
            }
            rightAnswerAlert(messageTitle: messageTitle, buttonTitle: buttonTitle)
        } else {
            print("No way")
            var messageTitle = AlertMessageStates.wrongAnswer.rawValue
            var buttonTitle = AlertButtonStates.nextQuestion.rawValue
            if isLastQuestion {
                messageTitle += AlertMessageStates.lastQuestion.rawValue
                buttonTitle = AlertButtonStates.lastQuestion.rawValue
            }
            wrongAnswerAlert(messageTitle: messageTitle, buttonTitle: buttonTitle)
        }
    }
    
    
    func startOver() {
        questionNumber = 0
        score = 0
        viewDidLoad()
    }
    
    func rightAnswerAlert(messageTitle: String, buttonTitle: String) {
        let alert = UIAlertController(title: "All right!", message: messageTitle, preferredStyle: .alert)
        let nextStep = UIAlertAction(title: buttonTitle, style:.default, handler: nil)
        alert.addAction(nextStep)
        present(alert, animated: true, completion: nil)
    }
    
    
    func wrongAnswerAlert(messageTitle: String, buttonTitle: String) {
        let alert = UIAlertController(title: "Oh no!", message: messageTitle, preferredStyle: .alert)
        let nextStep = UIAlertAction(title: buttonTitle, style:.default, handler: nil)
        alert.addAction(nextStep)
        present(alert, animated: true, completion: nil)
    }
    

    
}
